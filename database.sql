-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 07, 2018 at 06:11 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `db_images`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_images`
--

CREATE TABLE `t_images` (
  `id_image` bigint(20) UNSIGNED NOT NULL,
  `hash_image` varchar(64) NOT NULL,
  `tipe_image` varchar(32) NOT NULL,
  `md5_image` varchar(63) NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_produk` int(11) NOT NULL DEFAULT '0',
  `tgl_upload` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_upload` text,
  `ua_upload` varchar(160) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_images`
--

INSERT INTO `t_images` (`id_image`, `hash_image`, `tipe_image`, `md5_image`, `id_user`, `id_produk`, `tgl_upload`, `ip_upload`, `ua_upload`) VALUES
(1, 'G//Gx', 'png', 'd1bbb5420b212cb6d2770284b8b89dc2', 32, 43, '2018-10-25 17:17:48', '149.129.240.43', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_images`
--
ALTER TABLE `t_images`
  ADD PRIMARY KEY (`id_image`),
  ADD KEY `hash_image` (`hash_image`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_images`
--
ALTER TABLE `t_images`
  MODIFY `id_image` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

