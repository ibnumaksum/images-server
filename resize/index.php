<?php
//print_r($_GET);
//die();
/* Image Resizer
* Rekomendasi di server nginx
* http://sumitbirla.com/2011/11/how-to-build-a-scalable-caching-resizing-image-server/
* resize.php?i=foto/1/1.jpg&w=300&h=150&mode=2
*/

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');

ini_set("memory_limit","80M");
//ini_set("show_error",false);
include('SimpleImage.php');

# prevent creation of new directories
$is_locked = false;

$w = $_GET['w']; //width
$h = $_GET['h']; //height
$m = $_GET['m']; //tipe
$f = $_GET['f']; //folder
$g = $_GET['g']; //gambar

$ext = pathinfo($g, PATHINFO_EXTENSION); 
header("Content-Type: image/$ext");

//jika file asli tidak ada, hapus
if(!file_exists("../get/$f/$g")){
	header("HTTP/1.0 404 Not Found");
	echo "PHP continues.\n";
	die();
}
//buat Folder nya
$path = "";
if(!empty($w)){
	$path = __DIR__ . DIRECTORY_SEPARATOR . "$w";
	if(!file_exists($path)) mkdir($path);
	if(!empty($h)){
		$path = __DIR__ . DIRECTORY_SEPARATOR . "$w/$h";
		if(!file_exists($path)) mkdir($path);
		if(!empty($m)){
			$path = __DIR__ . DIRECTORY_SEPARATOR . "$w/$h/$m";
			if(!file_exists($path)) mkdir($path);
		}
	}else{
		$path = __DIR__ . DIRECTORY_SEPARATOR . "$w/$w";
		if(!file_exists($path)) mkdir($path);
		if(!empty($m)){
			$path = __DIR__ . DIRECTORY_SEPARATOR . "$w/$w/$m";
			if(!file_exists($path)) mkdir($path);
		}
	}
}else{
	header("HTTP/1.0 404 Not Found");
	echo "PHP continues..\n";
	die();
}
# check if new directory would need to be created
if(!file_exists("$path/$f/")) mkdir("$path/$f/");
$save_path = "$path/$f/$g";

$orig_file = "../get/$f/$g";
 
$new_width = $w;
if(!empty($h))
	$new_height = $h;
else
	$new_height = $w;

//$image = imagecreatefromjpeg($orig_file);

list($orig_width, $orig_height, $type, $attr) = getimagesize($orig_file);

$image = new abeautifulsite\SimpleImage($orig_file);
# Resize dan simpan di tengah ukuran yang di inginkan
if ($m == "1")
{
	$image->thumbnail($new_width, $new_height);
}
else if ($m == "2")
{
	$image->fit_to_width($new_width);
//}else if ($mode == "3")
//{
//	$image->fit_to_height($target_height);
# aspect ratio resize
}else{
	$image->best_fit($new_width, $new_height);
}

# save and return the resized image file        
$image->save($save_path,100,"jpg");
//$image->writeImage($save_path);
readfile($save_path);