<?php
ini_set('display_errors', 0);
include "medoo.min.php";

//untuk generate ID user
//mengganti ini bisa membuat doubleID
$hasimage = "Carsworld2018";

if(!isset($_SERVER["HTTP_REFERER"]) || $_SERVER["HTTP_REFERER"] != "https://carsworld.id"){
	if(file_exists($_FILES['photo']['tmp_name']))unlink($_FILES['photo']['tmp_name']);
	die(json_encode(array("status"=>"failed","message"=>'failed to upload image')));
	die();
}


$db = new medoo([
	// required
	'database_type' => 'mysql',
	'database_name' => 'db_images',
	'server' => 'localhost',
	'username' => 'images_user',
	'password' => 'QDMbBNc9mNJh1xFz',
	'charset' => 'utf8',
 
	// [optional]
	'port' => 3306,
 
	'option' => [
		PDO::ATTR_CASE => PDO::CASE_NATURAL
	]
]);

require '../vendor/autoload.php';

if($_FILES['photo']['name'])
{
	if(!$_FILES['photo']['error'])
	{
		if($_FILES["photo"]["type"]=="image/jpeg" || $_FILES["photo"]["type"]=="image/jpg"){
			
			if($_FILES['photo']['size'] > (10024000)) //can't be larger than 10 MB
			{
				if(file_exists($_FILES['photo']['tmp_name']))unlink($_FILES['photo']['tmp_name']);
				die(json_encode(array("status"=>"failed","message"=>'File size is to large.')));
			}else
			{	
				$md5 = md5_file($_FILES['photo']['tmp_name']);
				$data = $db->get('t_images',array("hash_image","tipe_image", "id_user"),array('md5_image'=>$md5));
				//jika gambar sama ada di database
				if(!empty($data)){
					if($data['id_user']==$_POST['uid']*1){
						//jika user sama
						die(json_encode(array("status"=>"success","image"=>$data['hash_image'].'.'.$data['tipe_image'])));
					}else{
						//jika user beda, tambahkan
						if($db->insert('t_images',array(
							'id_produk'=>$_POST['pid']*1,
							'id_user'=>$_POST['uid']*1,
							'hash_image'=>$data['hash_image'],
							'tipe_image'=>$data['tipe_image'],
							'md5_image'=>$md5,
							'tgl_upload'=>date("Y-m-d H:i:s"),
							'ip_upload'=>$_SERVER['REMOTE_ADDR'],
							'ua_upload'=>$_SERVER['HTTP_USER_AGENT']
							)))
							die(json_encode(array("status"=>"success","image"=>$data['hash_image'].'.'.$data['tipe_image'])));
						else
							die(json_encode(array("status"=>"failed","message"=>"Failed to upload images")));
						if(file_exists($_FILES['photo']['tmp_name']))unlink($_FILES['photo']['tmp_name']);
					}
				}else{
					//jika gambar baru
					$ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION); 
					if(empty($ext))
						$ext = str_replace("image/","",$_FILES["photo"]["type"]);
					$id = ($db->max('t_images', "id_image")*1)+1;
					$hashids = new Hashids\Hashids($hasimage);
					$hid = $hashids->encode($id);
					$alpa = '';
					if(strlen($hid)>1){
						$alpa = substr($hid,0,1)."/";
						if(!file_exists('../get/'.$alpa))
							mkdir('../get/'.$alpa);
					}
					if(move_uploaded_file($_FILES['photo']['tmp_name'], '../get/'.$alpa.$hid.".$ext")){
						if($db->insert('t_images',array(
							'id_image'=>$id,
							'id_produk'=>$_POST['pid']*1,
							'id_user'=>$_POST['uid']*1,
							'hash_image'=>$alpa.'/'.$hid,
							'tipe_image'=>$ext,
							'md5_image'=>$md5,
							'tgl_upload'=>date("Y-m-d H:i:s"),
							'ip_upload'=>$_SERVER['REMOTE_ADDR'],
							'ua_upload'=>$_SERVER['HTTP_USER_AGENT']
							)))
							die(json_encode(array("status"=>"success","image"=>$alpa.'/'.$hid.'.'.$ext)));
						else
							die(json_encode(array("status"=>"failed","message"=>'Error: failed to upload image')));
					}else{
						if(file_exists($_FILES['photo']['tmp_name']))unlink($_FILES['photo']['tmp_name']);
						die(json_encode(array("status"=>"failed","message"=>'Error: failed to upload image..')));
					}
				}
			}
		}else{
			if(file_exists($_FILES['photo']['tmp_name']))unlink($_FILES['photo']['tmp_name']);
			die(json_encode(array("status"=>"failed","message"=>'Please upload only jpeg/jpg')));
		}
	}
	//if there is an error...
	else
	{
		//set that to be the returned message
		if(file_exists($_FILES['photo']['tmp_name']))unlink($_FILES['photo']['tmp_name']);
		die(json_encode(array("status"=>"failed","message"=>'Error: Your upload triggered the following error:  '.$_FILES['photo']['error'])));
	}
}else
	die(json_encode(array("status"=>"failed","message"=>'No Data')));

#debug uncomment
// print_r($_FILES);
// print_r($_POST);
// print_r($db->log());
// print_r($db->error());